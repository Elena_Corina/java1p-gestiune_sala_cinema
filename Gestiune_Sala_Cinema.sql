CREATE DATABASE gestiune_sala_cinematograf;
USE gestiune_sala_cinematograf;

CREATE TABLE cinematograf(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
numar_sala INT NOT NULL
);

CREATE TABLE cont(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(50) NOT NULL UNIQUE,
parola VARCHAR(50) NOT NULL
);

CREATE TABLE rezervare(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nume_film VARCHAR(200) NOT NULL,
id_cont INT NOT NULL,
id_numar_sala INT NOT NULL,
FOREIGN KEY(id_cont) REFERENCES cont(id),
FOREIGN KEY (id_numar_sala) REFERENCES cinematograf(id)
);

INSERT INTO cinematograf(numar_sala) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9);

SELECT * FROM cinematograf;

SELECT * FROM cont;

SELECT * FROM rezervare;




