package view;

import controller.ContController;
import model.Cont;

import javax.swing.*;
import java.sql.SQLException;
import java.util.Optional;

public class ContFrame extends JFrame{
    private JTextField textField1;
    private JPasswordField passwordField1;
    private JButton loginButton;
    private JButton adaugareButton;
    private JPanel panelCont;
    private JFrame contFrame;


    public ContFrame(){
        contFrame = new JFrame("Cont");
        contFrame.add(panelCont);

        loginButton.addActionListener(ev -> login());
        adaugareButton.addActionListener(ev-> adaugare());

        contFrame.setSize(400, 200);
        contFrame.setLocationRelativeTo(null);
        contFrame.setVisible(true);
    }

    public void login(){
        String username = textField1.getText();
        String parola = new String(passwordField1.getPassword());
        Optional<Cont> cont = ContController.getInstance().login(username, parola);
        if(cont.isPresent()){
            Cont contLogat = cont.get();
            new RezervareFrame(contLogat);
            contFrame.dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Username sau parola gresite");
        }

    }

    private void adaugare() {
        String username = textField1.getText();
        String parola = new String(passwordField1.getPassword());

        if(username.equals("") || parola.equals("")) {
            JOptionPane.showMessageDialog(null, "introduceti un username si o parola");
        }else{
            if (!Character.isDigit(parola.charAt(parola.length() - 1))) {
                JOptionPane.showMessageDialog(null, "Parola trebuie sa se termine cu o cifra");
            } else {
                Cont cont = new Cont(username, parola);
                if (ContController.getInstance().adaugare(cont)) {
                    JOptionPane.showMessageDialog(null, "Contul a fost adaugat");
                } else {
                    JOptionPane.showMessageDialog(null, "Contul nu a fost adaugat");
                }
            }
        }
    }

}
