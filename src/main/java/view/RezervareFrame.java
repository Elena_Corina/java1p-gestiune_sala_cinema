package view;

import controller.ContController;
import controller.RezervareController;
import model.Cont;
import model.Rezervare;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class RezervareFrame extends JFrame{
    private JList<model.Rezervare> listaRezervari;
    private JTextField textField1;
    private JTextField textField2;
    private JPanel panelButoane;
    private JPanel mainPanel;
    private JButton adaugaProgramareButton;
    private JFrame rezervareFrame;
    private Cont cont;
    private DefaultListModel<Rezervare> model;

    public RezervareFrame(Cont cont){
        this.cont = cont;

        rezervareFrame = new JFrame("Rezervari");
        rezervareFrame.add(mainPanel);
        model = new DefaultListModel<>();
        listaRezervari.setModel(model);

        adaugaProgramareButton.addActionListener(ev -> adauga_programare());
        afiseaza_rezervarile();

        rezervareFrame.setSize(400,400);
        rezervareFrame.setLocationRelativeTo(null);
        rezervareFrame.setVisible(true);
    }

    private void adauga_programare(){
        String nume_film = textField1.getText();
        int numar_sala = Integer.parseInt(textField2.getText());
        int id_cont = cont.getId();

        Rezervare rezervare = new Rezervare(nume_film, id_cont, numar_sala);

        if (RezervareController.getInstance().adaugare_programare(rezervare)) {
            JOptionPane.showMessageDialog(null, "Rezervarea a fost adaugata");
            afiseaza_rezervarile();
        } else {
            JOptionPane.showMessageDialog(null, "Alegeti o sala de la 1-9");
        }
    }

    private void afiseaza_rezervarile( ){
        List<Rezervare> rezervari = null;
        rezervari = RezervareController.getInstance().afisareRezervari_byId(cont.getId());
        model.clear();
        rezervari.forEach(model::addElement);
    }

}
