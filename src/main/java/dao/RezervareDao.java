package dao;

import model.Rezervare;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RezervareDao {
    private Connection con;

    private PreparedStatement stmt1, stmt2;

    public RezervareDao(Connection con) {
        try {
            this.con = con;
            this.stmt1 = con.prepareStatement("SELECT * FROM rezervare WHERE id_cont = ?");
            this.stmt2 = con.prepareStatement("INSERT INTO rezervare VALUES (NULL, ?, ?, ?)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Rezervare> afisareRezervari() throws SQLException {
        List<Rezervare> rezervari = new ArrayList<>();
        //stmt1.setInt(1, "1");
        ResultSet rs = stmt1.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("id");
            String nume_film = rs.getString("nume_film");
            int id_cont = rs.getInt("id_cont");
            int id_numar_sala = rs.getInt("id_numar_sala");

            Rezervare rezervare = new Rezervare(id, nume_film, id_cont, id_numar_sala);
            rezervari.add(rezervare);
        }
        return rezervari;
    }

    public void adaugaRezervare(Rezervare rezervare) throws SQLException {
        stmt2.setString(1, rezervare.getNume_film());
        stmt2.setInt(2, rezervare.getId_cont());
        stmt2.setInt(3, rezervare.getId_numar_sala());
        stmt2.executeUpdate();
    }

    /*
    public Optional<Rezervare> findRezervare(int id_cont) throws SQLException {
        stmt1.setInt(1, id_cont);
        Rezervare rezervare = null;
        try(ResultSet rs = stmt1.executeQuery()){
            if(rs.next()){
                rezervare = new Rezervare();
                rezervare.setId(rs.getInt("id"));
                rezervare.setNume_film(rs.getString("nume_film"));
                rezervare.setId_cont(rs.getInt("id_cont"));
                rezervare.setId_numar_sala(rs.getInt("id_numar_sala"));
            }

        }
        return Optional.ofNullable(rezervare);
    }
*/

    public List<Rezervare> afisareRezervari_byId(int contId) throws SQLException {
        List<Rezervare> rezervari = new ArrayList<>();
        stmt1.setInt(1, contId);
        ResultSet rs = stmt1.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("id");
            String nume_film = rs.getString("nume_film");
            int id_numar_sala = rs.getInt("id_numar_sala");

            Rezervare rezervare = new Rezervare(id, nume_film, contId, id_numar_sala);
            rezervari.add(rezervare);
        }
        return rezervari;
    }

}
