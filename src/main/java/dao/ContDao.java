package dao;

import model.Cont;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class ContDao {
    private Connection con;

    private PreparedStatement stmt1, stmt2;

    public ContDao(Connection con) {
        try {
            this.con = con;
            this.stmt1 = con.prepareStatement("SELECT * FROM cont WHERE username = ?");
            this.stmt2 = con.prepareStatement("INSERT INTO cont VALUES (NULL, ?, ?)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Optional<Cont> findCont (String username) throws SQLException {
        stmt1.setString(1, username);
        Cont cont = null;
        try(ResultSet rs = stmt1.executeQuery()){
            if(rs.next()){
                cont = new Cont();
                cont.setId(rs.getInt("id"));
                cont.setUsername(rs.getString("username"));
                cont.setParola(rs.getString("parola"));
            }
        }
        return Optional.ofNullable(cont);
    }

    public void adaugaCont(Cont cont) throws SQLException {
        stmt2.setString(1, cont.getUsername());
        stmt2.setString(2, cont.getParola());
        stmt2.executeUpdate();
    }
}


