package controller;

import dao.RezervareDao;
import model.Rezervare;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class RezervareController {

    private RezervareDao rezervareDao;

    private static final class SingletonHolder{
        private static final RezervareController INSTANCE = new RezervareController();
    }

    private RezervareController(){
        rezervareDao = new RezervareDao(ConnectionManager.getInstance().getConnection());
    }

    public static RezervareController getInstance(){
        return  SingletonHolder.INSTANCE;
    }

    public List<Rezervare> afisareRezervari_byId(int id_cont){
        RezervareDao rezervareDao = new RezervareDao(ConnectionManager.getInstance().getConnection());
        try {
            return rezervareDao.afisareRezervari_byId(id_cont);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public boolean adaugare_programare(Rezervare rezervare){
        RezervareDao rezervareDao = new RezervareDao(ConnectionManager.getInstance().getConnection());
        boolean rez = false;
        try{
                rezervareDao.adaugaRezervare(rezervare);
                rez = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rez;
    }


}
