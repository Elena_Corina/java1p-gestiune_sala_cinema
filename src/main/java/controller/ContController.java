package controller;

import dao.ContDao;
import model.Cont;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

public class ContController {

    private ContDao contDao;

    private static final class SingletonHolder{
        private static final ContController INSTANCE = new ContController();

    }

    private ContController(){
        contDao = new ContDao(ConnectionManager.getInstance().getConnection());

    }
    public static ContController getInstance(){
        return SingletonHolder.INSTANCE;
    }

    public boolean adaugare(Cont cont){
        ContDao contDao = new ContDao(ConnectionManager.getInstance().getConnection());
        boolean rez = false;

        try{
            Optional<Cont> optionalCont = contDao.findCont(cont.getUsername());
            if(!optionalCont.isPresent()){
                contDao.adaugaCont(cont);
                rez = true;
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return rez;
    }

    public Optional<Cont> login(String username, String parola){
        ContDao contDao = new ContDao(ConnectionManager.getInstance().getConnection());
        try{
            Optional<Cont> optionalCont = contDao.findCont(username);

            if (optionalCont.isPresent()){
                if(optionalCont.get().getParola().equals(parola)){
                    return optionalCont;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
