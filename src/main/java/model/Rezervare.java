package model;

public class Rezervare {
    private int id;
    private String nume_film;
    private int id_cont;
    private int id_numar_sala;

    public Rezervare() {
    }

    public Rezervare(String nume_film, int id_cont, int id_numar_sala) {
        this.nume_film = nume_film;
        this.id_cont = id_cont;
        this.id_numar_sala = id_numar_sala;
    }

    public Rezervare(int id, String nume_film, int id_cont, int id_numar_sala) {
        this.id = id;
        this.nume_film = nume_film;
        this.id_cont = id_cont;
        this.id_numar_sala = id_numar_sala;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume_film() {
        return nume_film;
    }

    public void setNume_film(String nume_film) {
        this.nume_film = nume_film;
    }

    public int getId_cont() {
        return id_cont;
    }

    public void setId_cont(int id_cont) {
        this.id_cont = id_cont;
    }

    public int getId_numar_sala() {
        return id_numar_sala;
    }

    public void setId_numar_sala(int id_numar_sala) {
        this.id_numar_sala = id_numar_sala;
    }

    @Override
    public String toString() {
        return "Rezervare{" +
                "nume_film='" + nume_film + '\'' +
                ", id_numar_sala=" + id_numar_sala +
                '}';
    }
}
