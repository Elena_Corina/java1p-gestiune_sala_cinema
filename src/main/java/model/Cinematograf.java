package model;

public class Cinematograf {
    private int id;
    private int numar_sala;

    private Cinematograf() {
    }

    public int getId() {
        return id;
    }

    public int getNumar_sala() {
        return numar_sala;
    }

}
