**JAVA 1 Professional**

Se va realiza o aplicaţie care are ca scop gestiunea unei săli de cinematograf.
Orice persoană care vrea să ȋși rezerve bilet pentru film trebuie sa ȋși facă un cont. 
Pentru a face contul este nevoie ca aceasta să ȋși aleagă un username și o parolă. 
Ȋn baza de date nu trebuie să existe doi utilizatori cu același nume, iar parola trebuie să se termine cu o cifră. 
Pe lângă casetele de text fereastra pentru persoană va conţine două butoane, adăugare și logare. 
Butonul “Adăugare” va adăuga o nouă persoană ȋn baza de date.
Butonul de logare verifică dacă ȋn baza de date este o ȋntregistrare cu username- ul și parola introduse. 
Dacă persoana exista se deschide o fereastra de tip „Rezervări”, dacă nu se afișază mesajul „Username-ul sau parola gresita!”.
Rezervarea pentru un film se face specificând numele filmului și numărul sălii la care se dorește rezervarea. 
Cinematograful are 9 săli.
Fereastra „Rezervări” prezintă următoarele elemente:
• O listă a tuturor rezervărilor pentru persoana care s-a logat ȋn cont
• O casetă pentru numele filmului
• O casetă pentru numărul sălii la care se dorește rezervarea
• Un buton „Adaugă programare” care va adăuga ȋn baza de date o nouă
programare pentru utilizatorul care s-a logat.

Detalii de implementare:
➢ Respectarea principiilor SOLID de scriere al codului cât şi a standardelor de denumire.
➢ Aplicarea design patternurilor acolo unde este nevoie 
➢ Folosirea design patternului MVC